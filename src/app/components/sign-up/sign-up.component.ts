import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { IUser } from '../../models/user.model'
import { FormGroup, FormControl, FormBuilder } from '@angular/forms'
import { UserService } from '../../services/user.service'

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  public registerForm: FormGroup;

  myUsername: string = ''
  myEmail: string = ''
  myPassword: string = ''
  userCreated: boolean = false
  errorMsg: string = ''

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private http: HttpClient
  ) {
    this.registerForm = this.formBuilder.group({
      name: '',
      email: '',
      password: ''
    })
  }

  ngOnInit(): void {

  }

  submitBtn() {
    let objCreate = {
      name: this.registerForm.get('name')?.value,
      email: this.registerForm.get('email')?.value,
      password: this.registerForm.get('password')?.value
    }
    this.userService.createUser(objCreate)
      .subscribe(
        res => {
          this.userCreated = true
        },
        error => {
          this.errorMsg = `Something is wrong: ${error}`
        }
      )
  }

}
