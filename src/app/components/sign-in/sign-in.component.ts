import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms'
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { Store } from '@ngrx/store'
import { UserService } from '../../services/user.service'
import { IUserLoginResult } from '../../models/user-login-result.model'
import { setUser } from '../../reducers/user/user.actions'

interface UserState {
  user: IUserLoginResult
}

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  user: Observable<IUserLoginResult>;

  public loginForm: FormGroup;
  myEmail: string = ''
  myPassword: string = ''
  errorMsg = '';
  loadingProcess: boolean = false

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: Router,
    private store: Store<UserState>
  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    })
    this.user = this.store.select('user')
  }

  ngOnInit(): void {

  }

  // just for example data
  createUser() {
    this.store.dispatch(setUser({payload: { email: 'tian@gmail.com', access_token: 'askjdhaksjd'}}))
  }

  submitBtn() {
    // this.createUser()
    let email = this.loginForm.get('email')?.value
    let password = this.loginForm.get('password')?.value
    let objCreate: any = {
      email: email,
      password: password,
    }
    this.userService.loginUser(objCreate)
      .subscribe(
        res => {
          this.store.dispatch(setUser({ 
            payload: { email: email, access_token: res.access_token } 
          }))
          localStorage.setItem('userInfo', JSON.stringify({
            email: email,
            access_token: res.access_token
          }))
        },
        error => {
          this.errorMsg = `Something is wrong: ${error}`
        }
      )
  }

}
