import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { TopHeaderComponent } from './top-header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { RouterLinkWithHref } from '@angular/router';

describe('TopHeaderComponent', () => {
  let component: TopHeaderComponent;
  let fixture: ComponentFixture<TopHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({}),
        RouterTestingModule
      ],
      declarations: [ TopHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a link to /', () => {
    const debugElements = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref))
    const index = debugElements.findIndex(de => {
      return de.properties['href'] === '/'
    })
    expect(index).toBeGreaterThan(-1)
  })

  it('should have a link to /todo-list', () => {
    const debugElements = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref))
    const index = debugElements.findIndex(de => {
      return de.properties['href'] === '/todo-list'
    })
    expect(index).toBeGreaterThan(-1)
  })

  it('should have a link to /todo-input', () => {
    const debugElements = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref))
    const index = debugElements.findIndex(de => {
      return de.properties['href'] === '/todo-input'
    })
    expect(index).toBeGreaterThan(-1)
  })

  it('should have a link to /sign-in if not logged in', () => {
    if(component.showSignInBtn) {
      const debugElements = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref))
      const index = debugElements.findIndex(de => {
        return de.properties['href'] === '/sign-in'
      })
      expect(index).toBeGreaterThan(-1)
    } else {
      expect(fixture.debugElement.query(By.css('.signInBtn'))).toBeNull()
    }
  })

  it('should have a link to /sign-out if logged in', () => {
    if(component.showSignOutBtn) {
      const debugElements = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref))
      const index = debugElements.findIndex(de => {
        return de.properties['href'] === '/sign-up'
      })
      expect(index).toBeGreaterThan(-1)
    } else {
      expect(fixture.debugElement.query(By.css('.signUpBtn'))).toBeNull()
    }
  })

  it('should call logOut', () => {
    spyOn(component, 'logoutBtn').and.callThrough()
    component.logoutBtn()
    expect(component.logoutBtn).toHaveBeenCalled()
  })
});
