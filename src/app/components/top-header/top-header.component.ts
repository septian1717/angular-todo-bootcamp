import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store'
import { IUserLoginResult } from 'src/app/models/user-login-result.model';
@Component({
  selector: 'app-top-header',
  templateUrl: './top-header.component.html',
  styleUrls: ['./top-header.component.css']
})
export class TopHeaderComponent implements OnInit {

  showSignInBtn: boolean = true
  showSignOutBtn: boolean = true
  showLogoutBtn: boolean = false

  constructor(
    private router: Router,
    private store: Store<{ user: IUserLoginResult }>
  ) { }

  ngOnInit(): void {
    const userInfo = localStorage.getItem('userInfo')
    this.store.subscribe(state => {
      if(state.user?.email !== "" || userInfo !== null) {
        this.showLogoutBtn = true
        this.showSignInBtn = false
        this.showSignOutBtn = false
      }
    })
  }

  logoutBtn(): void {
    localStorage.clear();
    this.showLogoutBtn = false
    this.showSignInBtn = true
    this.showSignOutBtn = true
    this.router.navigate(['/'])
  }

}
