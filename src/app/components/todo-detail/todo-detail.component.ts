import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Location } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from 'src/app/services/todo.service';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit {

  // @ViewChild('firstDialog')

  deadline: string = ''
  description: string = ''
  done: boolean = false
  id: number = 0
  deleteStatus: boolean = false

  constructor(
    private location: Location,
    private router: Router,
    private todoService: TodoService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    try {
      const { itemContent } = (window.history.state) ? window.history.state : ''
      this.deadline = (itemContent.deadline) ? itemContent.deadline : ''
      this.description = (itemContent.description) ? itemContent.description : ''
      this.done = (itemContent.done) ? itemContent.done : ''
      this.id = (itemContent.id) ? itemContent.id : ''
    } catch (error) {
      this.router.navigate(['/todo-list'])
    }
  }

  openDialogWithRef(ref: TemplateRef<any>) {
    this.dialog.open(ref)
  }

  editTodo() {
    const { itemContent } = window.history.state || {}
    console.log(itemContent)
    this.router.navigate(['/todo-detail/edit'], {state: itemContent})
  }

  removeTodo(id: number): void {
    this.todoService.removeTodo(id)
      ?.subscribe(res => {
        this.deleteStatus = true
      })
  }

  goBack(): void {
    this.location.back()
  }

}
