import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service'
import { Router } from '@angular/router'
import {ITodoList} from '../../models/todo-list.model'

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todos: any = []
  loadingStatus: boolean = true
  defaultElevation = 2;
  raisedElevation = 8;

  constructor(
    private todoService: TodoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAllTodo()
  }

  getAllTodo(): void {
    this.todoService.getAllTodos()
      .subscribe(res => {
        this.todos = res
        this.loadingStatus = false
      })
  }

  toDetailPage(todo: ITodoList) {
    // console.log(todo)
    this.router.navigate(['/todo-detail', JSON.stringify(todo)])
  }

  inputTodo(): void {
    console.log('input todo')
  }

}
