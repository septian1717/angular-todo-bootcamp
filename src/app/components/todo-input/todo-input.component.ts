import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms'
import { MatDialog } from '@angular/material/dialog';
import { TodoService } from 'src/app/services/todo.service';

interface ITodoInput {
  description: string,
  deadline: string,
  done: boolean
}

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.css']
})
export class TodoInputComponent implements OnInit {

  @ViewChild('dialogInfo')
  dialogInfo!: TemplateRef<any>;

  public inputTodoForm: FormGroup;
  errorMsg: string = ''
  isDone: boolean = true
  date = new FormControl(new Date())
  minDate = new Date
  maxDate = new Date

  constructor(
    private formBuilder: FormBuilder,
    private todoService: TodoService,
    private dialog: MatDialog,
  ) {
    this.inputTodoForm = this.formBuilder.group({
      description: '',
      dateValue: '',
      doneValue: ''
    })
    let today:any = new Date().toLocaleDateString()
        today = new Date(today)
        today = new Date(today.getTime() + 86400000);
    this.minDate = today
  }

  ngOnInit(): void {
  }

  changeCheckbox() {
    this.isDone = !this.isDone
    console.log(!this.isDone)
  }

  submitBtn() {
    let description = this.inputTodoForm.get('description')?.value
    let deadline = this.inputTodoForm.get('dateValue')?.value
    let done = this.inputTodoForm.get('doneValue')?.value

    if(description == '' || deadline == '') {
      return alert('Please input the description and dealdine!')
    }

    let objCreate: ITodoInput = {
      description: description,
      deadline: deadline,
      done: done
    }
    this.todoService.createTodo(objCreate)
      .subscribe(res => {
        this.dialog.open(this.dialogInfo)
      })
  }

}
