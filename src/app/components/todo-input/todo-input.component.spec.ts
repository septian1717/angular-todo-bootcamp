import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { RouterTestingModule } from '@angular/router/testing';

import { TodoInputComponent } from './todo-input.component';

describe('TodoInputComponent', () => {
  let component: TodoInputComponent;
  let fixture: ComponentFixture<TodoInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatCheckboxModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: undefined }
      ],
      declarations: [TodoInputComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should click submit button when Insert Todo clicked', fakeAsync(() => {
    spyOn(component, 'submitBtn')
    let button = fixture.debugElement.nativeElement.querySelector('button')
    button.click()
    tick()
    expect(component.submitBtn).toHaveBeenCalled()
  }))
  
});
