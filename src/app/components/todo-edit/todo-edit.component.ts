import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.css']
})
export class TodoEditComponent implements OnInit {

  public inputTodoForm: FormGroup
  errorMsg: string = ''
  isDone: boolean = true
  date = new FormControl(new Date())

  constructor(
    private formBuilder: FormBuilder,
    private todoService: TodoService,
    private router: Router
  ) {
    const { description, done, deadline } = window.history.state || {}
    this.inputTodoForm = this.formBuilder.group({
      description: description || '',
      dateValue: deadline || '',
      doneValue: done || ''
    })
  }

  ngOnInit(): void {
    
  }

  changeCheckbox() {
    this.isDone = !this.isDone
  }

  submitBtn() {
    let objCreate: any = {
      description: this.inputTodoForm.get('description')?.value,
      deadline: this.inputTodoForm.get('dateValue')?.value,
      done: this.inputTodoForm.get('doneValue')?.value
    }
    this.todoService.createTodo(objCreate)
      .subscribe(res => {
        // this.dialog.open(this.dialogInfo)
      })
  }

}
