import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs'
import { retry, catchError, shareReplay } from 'rxjs/operators'
import { IUser } from '../models/user.model'
import { IUserSignUpRequest } from '../models/user-signup-request.model'
import { IUserLogin } from '../models/user-login.model'
import { MessageService } from './message.service'
import { Store } from '@ngrx/store'

import * as moment from 'moment'
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private store: Store
  ) { }

  private log(message: string) {
    this.messageService.add(`UserService: ${message}`)
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  httpError(error: any) {
    console.log(error)
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client side error
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.error.message}`;
    }
    console.log(msg);
    return throwError(msg);
  }

  setUser() {
    let dataSession = {
      email: 'tes@gmail.com',
      access_token: '12391827398172391283'
    }
    // this.store.dispatch(setUser({ user: dataSession }))
  }


  createUser(user: IUser): Observable<IUserSignUpRequest> {
    let url = 'https://cdc-todo-be.herokuapp.com/auth/signup'
    let bodySend = {
      "name": user.name,
      "email": user.email,
      "password": user.password
    }
    return this.http.post<IUserSignUpRequest>(url, bodySend, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  loginUser(user: IUser): Observable<IUserLogin> {
    let url = 'https://cdc-todo-be.herokuapp.com/auth/signin'
    let bodySend = {
      "username": user.email,
      "password": user.password
    }
    return this.http.post<IUserLogin>(url, bodySend, this.httpOptions)
      .pipe(
        shareReplay(1),
        catchError(this.httpError)
      )
  }
}
