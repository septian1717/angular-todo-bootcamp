import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs'
import { retry, catchError } from 'rxjs/operators'
import { ITodoRequestList } from '../models/todo-list-request.model'
import { ITodoList } from '../models/todo-list.model'
@Injectable({
  providedIn: 'root'
})
export class TodoService {

  public todos: ITodoRequestList[] = []
  public baseUrl = `https://cdc-web-frontend.herokuapp.com/todos`

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  httpError(error: any) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client side error
      msg = error.error.message;
    } else {
      // server side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(msg);
    return throwError(msg);
  }

  getAllTodos(): Observable<ITodoRequestList[]> {
    return this.http.get<ITodoRequestList[]>(this.baseUrl, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  removeTodo(idTodo: number) {
    return this.http.delete(`${this.baseUrl}/${idTodo}`)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  updateTodo(todo: ITodoRequestList, idTodo: number) {
    let bodySend = {
      description: todo.description,
      deadline: todo.deadline,
      done: todo.done
    }
    return this.http.patch(`${this.baseUrl}/${idTodo}`, bodySend, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  createTodo(todo: ITodoList): Observable<ITodoRequestList> {
    let bodySend = {
      description: todo.description,
      deadline: todo.deadline,
      done: todo.done
    }
    return this.http.post<ITodoRequestList>(`${this.baseUrl}`, bodySend, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }
}
