import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { TodoService } from './todo.service';

describe('TodoService', () => {
  let injector: TestBed
  let service: TodoService;
  let httpMock: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TodoService]
    });
    // service = TestBed.inject(TodoService);
    injector = getTestBed()
    service = injector.get(TodoService)
    httpMock = injector.get(HttpTestingController)
  });

  afterEach(() => {
    httpMock.verify()
  });
  
  describe('#getAllTodos', () => {
    it('should return an Observable<Todo[]>', () => {
      const dummyTodo = [
        {
          id: 1,
          description: 'description',
          deadline: '2019-09-19T07:45:51.377Z',
          done: false
        }
      ]
      service.getAllTodos().subscribe(todo => {
        expect(todo.length).toBe(1)
        expect(todo).toEqual(dummyTodo)
      })

      const req = httpMock.expectOne(`${service.baseUrl}`)
      expect(req.request.method).toBe("GET")
      req.flush(dummyTodo)
    })
  })

  describe('#removeTodo', () => {
    it('should return 204 (no content) when remove todo', () => {
      service.removeTodo(3).subscribe()
      const req = httpMock.expectOne(`${service.baseUrl}/${3}`)
      expect(req.request.method).toBe("DELETE")

      // respond with 204
      const msg = '204'
      req.flush(msg, { status: 204, statusText: 'No Content' })
    })
  })

  describe('#updateTodo', () => {
    const dummyTodo = {
      id: 1,
      description: 'description',
      deadline: '2019-09-19T07:45:51.377Z',
      done: false
    }
    it('should return 204 when edit todo', () => {
      service.updateTodo(dummyTodo, 3).subscribe()
      const req = httpMock.expectOne(`${service.baseUrl}/3`)
      expect(req.request.method).toBe("PATCH")

      const msg = '204'
      req.flush(msg, { status: 204, statusText: 'No Content' })
    });

  });

  describe('#createTodo', () => {

    it('should return <Todo[]> when create Todo', async (done) => {
      const dummyTodo = {
        description: 'description',
        deadline: '2019-09-19T07:45:51.377Z',
        done: false
      }
      service.createTodo(dummyTodo)
        .subscribe(res => {
          expect(res).toEqual(dummyTodo);
          done()
        })
        const mockResponse = dummyTodo
        const req = httpMock.expectOne(`${service.baseUrl}`)
        expect(req.request.method).toEqual('POST')
        req.flush(mockResponse)
    });
  });


});
