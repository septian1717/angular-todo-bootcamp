import { Action, createAction, props, createSelector } from '@ngrx/store'
import { IUserLoginResult } from '../../models/user-login-result.model'

export const setUser = createAction(
    '[User] CreateUser',
    props<{ payload: IUserLoginResult}>()
);

export const resetUser = createAction('[User] ResetUser');

export const selectUser = (state: IUserLoginResult) => state

export const getUser = createSelector(
    selectUser,
    (state: IUserLoginResult) => state
);