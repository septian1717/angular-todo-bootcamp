import * as UserAction from './user.actions'
import { createReducer, on } from '@ngrx/store';
import { IUserLoginResult } from '../../models/user-login-result.model'

const initialState: IUserLoginResult = {
    email: '',
    access_token: ''
};

export const userReducer = createReducer(
    initialState,
    on(
        UserAction.setUser,
        (state, { payload } ) => ({ 
            email: payload.email, access_token: payload.access_token 
        }),
    ),
    on(
        UserAction.resetUser,
        (state) => initialState
    )
);
