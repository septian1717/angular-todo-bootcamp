export interface ITodoRequestList {
    id?: number,
    description: string,
    deadline: string,
    done: boolean
}