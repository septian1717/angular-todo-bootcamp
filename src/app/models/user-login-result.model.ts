export interface IUserLoginResult {
    email: string,
    access_token: string
}