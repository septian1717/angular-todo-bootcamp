export interface IUserSignUpRequest {
    id: string,
    name: string,
    email: string
}