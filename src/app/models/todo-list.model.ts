export interface ITodoList {
    description: string,
    deadline: string,
    done: boolean
}