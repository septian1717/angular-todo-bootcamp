import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { FlexLayoutModule } from '@angular/flex-layout';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MaterialModule } from 'src/app/shared/modules/material.module'

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TopHeaderComponent } from './components/top-header/top-header.component';
import { SignUpComponent } from './components/sign-up/sign-up.component'
import { SignInComponent } from './components/sign-in/sign-in.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoInputComponent } from './components/todo-input/todo-input.component';
import { TodoDetailComponent } from './components/todo-detail/todo-detail.component';
import { TodoEditComponent } from './components/todo-edit/todo-edit.component';

import { StoreModule } from '@ngrx/store'
import { counterReducer } from './reducers/counter/counter.reducer';
import { CounterComponent } from './components/counter/counter.component'

import { environment } from '../environments/environment'
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { userReducer } from './reducers/user/user.reducer';
@NgModule({
  declarations: [
    TodoListComponent,
    SignUpComponent,
    SignInComponent,
    AppComponent,
    TopHeaderComponent,
    TodoInputComponent,
    TodoDetailComponent,
    TodoEditComponent,
    CounterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MaterialModule,
    StoreModule.forRoot({ count: counterReducer, user: userReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
      autoPause: true
    })
  ],
  providers: [
    {
      provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
      useValue: { useUtc: true }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
