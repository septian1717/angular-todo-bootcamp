import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CounterComponent } from './components/counter/counter.component';
import { AuthGuardService as AuthGuard } from './components/sign-in/auth-guard.service';
import { SignInComponent } from './components/sign-in/sign-in.component'
import { SignUpComponent } from './components/sign-up/sign-up.component'
import { TodoDetailComponent } from './components/todo-detail/todo-detail.component'
import { TodoEditComponent } from './components/todo-edit/todo-edit.component';
import { TodoInputComponent } from './components/todo-input/todo-input.component';
import { TodoListComponent } from './components/todo-list/todo-list.component'


const routes: Routes = [
  {
    path: '',
    redirectTo: 'todo-list',
    pathMatch: 'full',
  },
  {
    path: 'todo-list',
    component: TodoListComponent
  },
  {
    path: 'todo-detail/edit',
    component: TodoEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'todo-input',
    component: TodoInputComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'todo-detail',
    component: TodoDetailComponent
  },
  {
    path: 'counter',
    component: CounterComponent
  },
  {
    path: 'sign-in',
    component: SignInComponent,
    // canActivate: [!AuthGuard]
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    // canActivate: [!AuthGuard]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
