import { NgModule } from '@angular/core';

import { MatSliderModule } from '@angular/material/slider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from 'src/app/app-routing.module';

@NgModule({
    imports: [
        MatSliderModule,
        MatToolbarModule,
        MatInputModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatCardModule,
        MatIconModule,
        MatNativeDateModule,
        MatGridListModule,
        MatButtonModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatDatepickerModule,
        MatMomentDateModule,
        FlexLayoutModule
    ],
    exports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule,
        MatSliderModule,
        MatToolbarModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatCardModule,
        MatIconModule,
        MatNativeDateModule,
        MatGridListModule,
        MatButtonModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatDatepickerModule,
        MatMomentDateModule        
    ]
})

export class MaterialModule {}